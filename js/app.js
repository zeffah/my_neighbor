/*
**Main app js file
*/
'use strict';
// declare modules
// angular.module('myNeighbor', []);
// angular.module('Home', []);
var myApp = angular.module('myNeighbor', ['ui.router', 'ngCookies', 'datatables', 'ngMaterial']);
	myApp.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise('/login');
		$stateProvider
		.state('home',{
			url: '/',
			views: {
				'': {templateUrl: './views/home.html', controller: 'HomeController'},
				'nav@home': {templateUrl: './partials/nav.html'},
				'body@home':{templateUrl: './partials/body.html'},
				'footer@home':{templateUrl: './partials/footer.html'}
			}
		})
		.state('login',{
			url: '/login',
			views: {
				'': {templateUrl: './views/login.html', controller: 'LoginController'}
			}
		})
		.state('landlord-dash',{
			url: '/landlord_control',
			views: {
				'': {templateUrl: './views/landlord-dash.html', controller: 'DashController'},
				'nav@landlord-dash': {templateUrl: './partials/landlord_nav.html', controller: 'DashController'}
			}
		})
		.state('landlord-dash.new_house',{
			url: '/new_house',
			views: {
				'': {templateUrl: './views/new_house.html', controller: 'HousesController'}
				// 'nav@landlord-dash': {templateUrl: './partials/admin-nav.html', controller: 'DashController'}
			}
		})
		.state('landlord-dash.agent_message',{
			url: '/agent_message',
			views: {
				'': {templateUrl: './views/admin-communication.html', controller: 'HomeController'}
				// 'tabs@agent-dash.reports': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('landlord-dash.landlord_complains',{
			url: '/complains',
			views: {
				'': {templateUrl: './views/landlord_complains.html', controller: 'HousesController'},
				'land_tabs@landlord-dash.landlord_complains': {templateUrl: './partials/land_tabs.html', controller: 'HousesController'}
			}
		})
		.state('landlord-dash.landlord_recommendations',{
			url: '/recommendations',
			views: {
				'': {templateUrl: './views/landlord_recommendation.html', controller: 'HousesController'},
				'land_tabs@landlord-dash.landlord_recommendations': {templateUrl: './partials/land_tabs.html', controller: 'HousesController'}
			}
		})
		.state('landlord-dash.landlord_updates',{
			url: '/updates',
			views: {
				'': {templateUrl: './views/landlord_updates.html', controller: 'HousesController'},
				'land_tabs@landlord-dash.landlord_updates': {templateUrl: './partials/land_tabs.html', controller: 'HousesController'}
			}
		})
		.state('agent-dash',{
			url: '/agent-dash',
			views: {
				'': {templateUrl: './views/agent-dash.html', controller: 'DashController'},
				'nav@agent-dash': {templateUrl: './partials/admin-nav.html', controller: 'DashController'}
			}
		})
		.state('agent-dash.broadcast',{
			url: '/broadcast_messages',
			views: {
				'': {templateUrl: './views/broadcast.html', controller: 'broadcastController'}
			}
		})
		.state('agent-dash.complains',{
			url: '/complains',
			views: {
				'': {templateUrl: './views/complains.html', controller: 'broadcastController'},
				'tabs@agent-dash.complains': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('agent-dash.recommendations',{
			url: '/recommendations',
			views: {
				'': {templateUrl: './views/recommendations.html', controller: 'broadcastController'},
				'tabs@agent-dash.recommendations': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('landlord-dash.landlord_reports',{
			url: '/landlord_reports',
			views: {
				'': {templateUrl: './views/landlord_reports.html', controller: 'HousesController'},
				'land_tabs@landlord-dash.landlord_reports': {templateUrl: './partials/land_tabs.html', controller: 'HousesController'}
			}
		})
		.state('agent-dash.updates',{
			url: '/updates',
			views: {
				'': {templateUrl: './views/updates.html', controller: 'broadcastController'},
				'tabs@agent-dash.updates': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('agent-dash.new_tenant',{
			url: '/tenant',
			views: {
				'': {templateUrl: './views/new-tenant.html', controller: 'tenantsController'}
			}
		})
		.state('agent-dash.reports',{
			url: '/reports',
			views: {
				'': {templateUrl: './views/reports.html', controller: 'broadcastController'},
				'tabs@agent-dash.reports': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('agent-dash.broadcast_to_tenants',{
			url: '/to_tenants',
			views: {
				'': {templateUrl: './views/msg_to_tenant.html', controller: 'HomeController'}
				// 'tabs@agent-dash.reports': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		})
		.state('agent-dash.landlord_message',{
			url: '/landlord_message',
			views: {
				'': {templateUrl: './views/landlord_message.html', controller: 'HomeController'}
				// 'tabs@agent-dash.reports': {templateUrl: './partials/tabs.html', controller: 'broadcastController'}
			}
		});
	}]);

	myApp.run(['$rootScope', '$location', '$cookieStore', '$http', 'DTOptionsBuilder',
    function ($rootScope, $location, $cookieStore, $http, DTOptionsBuilder) {

        // DataTables configurable options
        $rootScope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(7)
        .withOption('bLengthChange', true);

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
            console.log($rootScope.globals.currentUser.authdata);
        }
  
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
        });
    }]);