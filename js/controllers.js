/*
**Project controllers
*/
'use strict';

var app = angular.module("myNeighbor");
	app.controller("LoginController", ['$scope', '$state', '$rootScope', 'AuthenticationService', 'Share', 'IP_ADDRESS',
	 function($scope, $state, $rootScope, AuthenticationService, Share, IP_ADDRESS){
	 	// reset login status
        AuthenticationService.ClearCredentials();

        $scope.login = function(){
        	$scope.dataLoading = true;
            var loginData = {username: $scope.username, password: $scope.password, role:$scope.role};
            console.log(loginData);
        	AuthenticationService.Login($scope.username, $scope.password, $scope.role, function(response) {
                console.log(response.userData);
                var user = response.userData;
                if(response.error.code == 1) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password, $scope.role);
                    Share.username.push($scope.username);

                    window.localStorage['user'] = JSON.stringify(user);
                    switch($scope.role){
                    	case 'agent':
                    	$state.go('agent-dash');
                    	break;
                    	case 'landlord':
                    	$state.go('landlord-dash');
                    	break;
                    	default:
                    	"Invalid request";
                    	break;
                    }
                } else {
                    $scope.error = "Something went wrong! Please try again";
                    $scope.dataLoading = false;
                }
            });
        };
	}]);

	app.controller("HomeController", function($scope, $state, $http, IP_ADDRESS, $window){
        $scope.currentUser = JSON.parse(window.localStorage['user']).role;
        console.log($scope.currentUser);
        $scope.reloadRoute = function(){
            $state.reload();
        }

        /*------------------------------------------------
                        Broadcast to tenants
        .................................................*/
        $scope.sendToTenants = function(){
            // console.log($scope.message+' and '+ JSON.parse(window.localStorage['user']).id+ " "+$scope.recipient);
            $http.post(IP_ADDRESS.ip+'broadcast_to_tenants', 
                {content: $scope.message, sender: JSON.parse(window.localStorage['user']).id, recipient: $scope.recipient})
            .then(function(response){
                $state.go($state.current, {}, {reload: true})
            }, function(error){
                console.log(error);
            });
        }
        /**..............END OF BROADCAST..................**/

        $http.post(IP_ADDRESS.ip+'list_houses',
            {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                var hList = Array();
                for (var i = 0; i < response.length; i++) {
                    hList.push(response[i]);
                }
                $scope.Landlords = hList;
            })
            .error(function(){
                console.log('Error');
            });

        /*--------------------------------------------------
        *Retrieve communication between agent and landlord*
        *---------------------------------------------------*/
        $scope.landlordMsgs = Array();
        $http.post(IP_ADDRESS.ip+'admin_comm', {user_id: JSON.parse(window.localStorage['user']).id})
        .then(function(response){
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].role == 'agent') {
                    $scope.landlordMsgs.push(response.data[i]);
                }
            }
        }, function(error){
            console.log(error);
        });
        /**--------------------------------------------------------**/
        $scope.myAgents = function(){
            $http.post(IP_ADDRESS.ip+'myAgents',
             {landlord_id: JSON.parse(window.localStorage['user']).id})
            .then(function(response){
                $scope.Agents = response.data;
            },
            function(error){
                console.log(error);
            });
        }
        /*
        ** My agents
        */
        $scope.myAgents();
        
        $scope.openModal = function(){
            $('#sendMsg').openModal();
        }

        $scope.sendAdminMsg = function(){
            $http.post(IP_ADDRESS.ip+'admin_message', 
                {title: $scope.message_title, message: $scope.message_content, sender: JSON.parse(window.localStorage['user']).id, recipient: $scope.agList})
            .then(function(success){
                $state.go($state.current, {}, {reload: true});
            }, function(error){
                console.log(error);
            });
        }

        $scope.sendAdmnMsg = function(){
            $http.post(IP_ADDRESS.ip+'admin_message', 
                {title: $scope.msg_title, message: $scope.msg_content, sender: JSON.parse(window.localStorage['user']).id, recipient: $scope.landlordsList})
            .then(function(success){
                $state.go($state.current, {}, {reload: true});
            }, function(error){
                console.log(error);
            });
        }

	});

    app.controller("DashController", function($scope, $http, $state, AuthenticationService, Share, IP_ADDRESS, $window){
        $scope.reloadRoute = function(){
            $state.reload();
        }

        $scope.logout = function(){
            AuthenticationService.ClearCredentials();
            window.localStorage['username'] = {};
            $state.go('home');
        };

        $scope.username = JSON.parse(window.localStorage['user']).username;

        /*
        **List the houses assigned to agent
        */
        $scope.listHouses = function(){
            $http.post(IP_ADDRESS.ip+'list_houses',
            {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                var hList = Array();
                for (var i = 0; i < response.length; i++) {
                    hList.push(response[i]);
                }
                $scope.houseList = hList;
            })
            .error(function(){
                console.log('Error');
            });
        }
        $scope.listHouses();

        //List of all tenants belonging to a specific agency
        $scope.agent_tenants = function(){
            $http.post(IP_ADDRESS.ip+'agent_tenants',
            {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                var tList = Array();
                for (var i = 0; i < response.length; i++) {
                    tList.push(response[i]);
                }
                $scope.tenantsList = tList;

                console.log($scope.tenantsList);
            })
            .error(function(){
                console.log('Error');
            });
        }
        $scope.agent_tenants();

        /*
        **function to delete tenant
        */
        $scope.delTenant = function(id){
            var result = confirm("You sure to delete?");
            if(result){
                $http.post(IP_ADDRESS.ip+'del_tenant', {id: id.tenantID})
                .then(function(res){
                    if(res.data.success == "1"){
                        //something
                        $http.post(IP_ADDRESS.ip+'update_unit', {unit_id: id.unitID, status: 0})
                        .success(function(data){
                            $window.alert('Action passed!');
                            $state.go($state.current, {}, {reload: true});

                        })
                        .error(function(error){
                            console.log(error);
                        });
                    }

                });
            }
        };

        $scope.modalOpen = function(data){
            $scope.et_id = data.tenantID;
            $scope.unit_id = data.unitID;

            $http.post(IP_ADDRESS.ip+'selectTenant',
            {agent_id: JSON.parse(window.localStorage['user']).id, tenant_id: data.tenantID})
            .success(function(response){
            
                $scope.phoneNumber = response.phone;
                $('#editTnt').openModal();
            })
            .error(function(){
                console.log('Error');
            });
        }

         /*......EDIT TENANT........*/
        $scope.editTenant = function(data){
            console.log(data);
            $http.post(IP_ADDRESS.ip+'editTenant', {id: $scope.et_id, phone: data.phone, house: data.house, unit: data.unit})
            .then(function(response){
                console.log(response);
                console.log(data.unit);
                // if (response.code == '1') {
                    $http.post(IP_ADDRESS.ip+'update_unit', {unit_id: $scope.unit_id, status: 0})
                    .success(function(reply){
                        $http.post(IP_ADDRESS.ip+'update_unit', {unit_id: data.unit, status: 1})
                        .success(function(resp){
                            $window.alert('You have Successfully edited!');
                            $state.go($state.current, {}, {reload: true});
                        })
                        .error(function(error){
                            console.log(error);
                        });
                    })
                    .error(function(error){
                        console.log(error);
                    });
                // }
            }, function(error){
                console.log(error);
            });
        }
        /*.........END.............*/

        /*
        ** Select ALL units under selected selected house
        */
        $scope.getUnits = function(house_id){
            $scope.id = house_id;

            $http.post(IP_ADDRESS.ip+'list_units', {house_id: house_id})
            .success(function(data){
                var uList = Array();
                for (var i = 0; i < data.length; i++) {
                    uList.push(data[i]);
                }
                $scope.unitList = uList;
            })
            .error(function(error){
                console.log(error);
            })
        }

        $scope.myAgents = function(){
            $http.post(IP_ADDRESS.ip+'myAgents',
             {landlord_id: JSON.parse(window.localStorage['user']).id})
            .then(function(response){
                $scope.Agents = response.data;
            },
            function(error){
                console.log(error);
            });
        }
        /*
        ** My agents
        */
        $scope.myAgents();

        $scope.delHouse = function(info){
            console.log(info.id)
            var result = confirm("You sure to delete?");
            if(result){
                $http.post(IP_ADDRESS.ip+'reassignHouse', {
                id: parseInt(info.id)})
                .success(function(data){
                    $http.post(IP_ADDRESS.ip+'update_house', 
                        {house_id: parseInt(info.houseID), status: 0})
                    .then(function(success){
                        $window.alert('Delete Successful');
                    $state.go($state.current, {}, {reload: true});
                    },
                    function(error){
                        console.log(error);
                    });

                })
                .error(function(error){
                    console.log(error);
                });
            }
        }
        
        $scope.openModal = function(id){
            $('#reply').openModal();
            window.localStorage['broadcast_id'] = id;
        }

    });

    app.controller('tenantsController', function($scope, $http, IP_ADDRESS, $window, $state){

        $scope.reloadRoute = function(){
            $state.reload();
        }

        $scope.tntRegister = function(){

            $http.post(IP_ADDRESS.ip+'register_tenant', 
            {
                firstname: $scope.firstname,
                lastname: $scope.lastname,
                phone: $scope.phone,
                nationalID: $scope.national_id,
                house_id: $scope.house,
                unit_id: $scope.unit
            })
            .success(function(response){
                $http.post(IP_ADDRESS.ip+'update_unit', {unit_id: response.unitID, status: 1})
                .success(function(data){
                    $window.alert('You have Successfully created a new tenant!');
                    $state.go('agent-dash');
                })
                .error(function(error){
                    console.log(error);
                });

            })
            .error(function(error){

            });
        };

        /*
        ** Select ALL units under selected selected house
        */
        $scope.getUnits = function(house_id){
            $scope.id = house_id;
            console.log($scope.id);

            $http.post(IP_ADDRESS.ip+'list_units', {house_id: house_id})
            .success(function(data){
                var uList = Array();
                for (var i = 0; i < data.length; i++) {
                    uList.push(data[i]);
                }
                $scope.unitList = uList;
            })
            .error(function(error){
                console.log(error);
            })
        }
        /*
        **Enroll tenant 
        */

    });

    app.controller('HousesController', function($http, $scope, IP_ADDRESS, $state, $window){

        $scope.landlord_complains = function(){
            $http.post(IP_ADDRESS.ip+'complains_for_landlord', 
                {landlord_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantComplains = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.landlord_complains();

        $scope.landlord_updates = function(){
            $http.post(IP_ADDRESS.ip+'updates_for_landlord', 
                {landlord_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantUpdates = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.landlord_updates();

        $scope.landlord_recommendations = function(){
            $http.post(IP_ADDRESS.ip+'recommendations_for_landlord', 
                {landlord_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantRecommendations = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.landlord_recommendations();



        $scope.reloadRoute = function(){
            $state.reload();
        }

        $http.get(IP_ADDRESS.ip+'list_agents')
        .success(function(response){
            var mList = Array();
            for (var i = 0; i < response.length; i++) {
                mList.push(response[i]);
            }
            $scope.agentList = mList;
        })
        .error(function(){
            console.log('Error');
        });

          /*
        ** List my houses (for belonging to landlords but not assigned yet)
        */
        $http.post(IP_ADDRESS.ip+'my_houses',
        {landlord_id: JSON.parse(window.localStorage['user']).id})
        .success(function(response){
            var myList = Array();
            for (var i = 0; i < response.length; i++) {
                myList.push(response[i]);
            }
            $scope.mHseList = myList;
        })
        .error(function(){
            console.log('Error');
        });

          /*
        ** List my houses (for belonging to landlords both assigned and not assigned)
        */
        $http.post(IP_ADDRESS.ip+'landlordHouses',
        {landlord_id: JSON.parse(window.localStorage['user']).id})
        .success(function(response){
            var myList = Array();
            for (var i = 0; i < response.length; i++) {
                myList.push(response[i]);
            }
            $scope.allHouses = myList;
        })
        .error(function(){
            console.log('Error');
        });

        $scope.houseUnits = function(){
            console.log($scope.houses+ ' : '+ $scope.unit_no);
            $http.post(IP_ADDRESS.ip+'register_units', 
            {
                house_id: $scope.houses, 
                unit: $scope.unit_no, 
                status: 0
            })
            .success(function(tick){
                $window.alert('Action passed!');
                $state.go($state.current, {}, {reload: true});
            })
            .error(function(fail){
                console.log(fail);
            });
        };

        $scope.assignHouse = function(){
            var dt = {house_id: $scope.house, agent_id: $scope.agents,
            landlord_id: JSON.parse(window.localStorage['user']).id};

            $http.post(IP_ADDRESS.ip+'assign_house', {
            agent_id: parseInt($scope.agents), 
            house_id: parseInt($scope.house),
            landlord_id: JSON.parse(window.localStorage['user']).id
            })
            .success(function(data){
                $http.post(IP_ADDRESS.ip+'update_house', 
                    {house_id: parseInt($scope.house), status: 1})
                .then(function(success){
                    $window.alert('Successfully assigned your house to an agent!');
                    $state.go($state.current, {}, {reload: true});
                },
                function(error){
                    console.log(error);
                });
            })
            .error(function(error){
                console.log(error);
            });

        }

        /*
        ** Register a new house
        */
        $scope.myHouses = function(){
            var dt = {houseName: $scope.housename, capacity: $scope.capacity, landlord: JSON.parse(window.localStorage['user']).id};
            console.log(dt);

            $http.post(IP_ADDRESS.ip+'register_house', {
            capacity: parseInt($scope.capacity), 
            houseName: $scope.housename,
            landlord: JSON.parse(window.localStorage['user']).id
            })
            .success(function(data){
                    $window.alert('A house was Successfully created ');
                    $state.go($state.current, {}, {reload: true});
            })
            .error(function(error){
                console.log(error);
            });
        }
    });

    app.controller('broadcastController', function($http, $scope, $state, IP_ADDRESS, $window){

        $scope.reloadRoute = function(){
            $state.reload();
        }

        $scope.printDocument = function(){
          var pdf = new jsPDF('p','pt','a4');
          pdf.addHTML($("#report").get(0),  function() {
            pdf.save('web.pdf');
          });
        }

        //list all broadcast that needs reply
        $scope.broadCast = function(){
            $http.post(IP_ADDRESS.ip+'broadcast', 
                {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.msgs = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.broadCast();

        /**---------------------------------------------------
            Retrieve communication between agent and landlord**/
        $scope.agentMsgs = Array();
        $http.post(IP_ADDRESS.ip+'admin_comm', {user_id: JSON.parse(window.localStorage['user']).id})
        .then(function(response){
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].role != 'agent') {
                    $scope.agentMsgs.push(response.data[i]);
                }
            }
        }, function(error){
            console.log(error);
        });
        /*--------------------------------------------------*/

        /*
        ** function for listing complains
        */
        $scope.complains = function(){
            $http.post(IP_ADDRESS.ip+'complains', 
                {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantComplains = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.complains();

        // $scope.landlord_complains = function(){
        //     $http.post(IP_ADDRESS.ip+'complains_for_landlord', 
        //         {agent_id: JSON.parse(window.localStorage['user']).id})
        //     .success(function(response){
        //         $scope.tenantComplains = response;
        //     })
        //     .error(function(error){
        //         console.log(error);
        //     })
        // }
        // $scope.landlord_complains();

        //list updates from tenants
        $scope.updates = function(){
            $http.post(IP_ADDRESS.ip+'updates', 
                {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantUpdates = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.updates();

        // $scope.landlord_updates = function(){
        //     $http.post(IP_ADDRESS.ip+'updates_for_landlord', 
        //         {agent_id: JSON.parse(window.localStorage['user']).id})
        //     .success(function(response){
        //         $scope.tenantUpdates = response;
        //     })
        //     .error(function(error){
        //         console.log(error);
        //     })
        // }
        // $scope.landlord_updates();

        //list all recommendations from tenants
        $scope.recommendations = function(){
            $http.post(IP_ADDRESS.ip+'recommendations', 
                {agent_id: JSON.parse(window.localStorage['user']).id})
            .success(function(response){
                $scope.tenantRecommendations = response;
            })
            .error(function(error){
                console.log(error);
            })
        }
        $scope.recommendations();

        // $scope.landlord_recommendations = function(){
        //     $http.post(IP_ADDRESS.ip+'recommendations_for_landlord', 
        //         {agent_id: JSON.parse(window.localStorage['user']).id})
        //     .success(function(response){
        //         $scope.tenantRecommendations = response;
        //     })
        //     .error(function(error){
        //         console.log(error);
        //     })
        // }
        // $scope.landlord_recommendations();

        //Reply to broadcast
        $scope.reply = function(){
            console.log(window.localStorage['broadcast_id']);
            $http.post(IP_ADDRESS.ip+'reply', 
                {reply:$scope.msg_reply, msg_id: window.localStorage['broadcast_id']})
            .success(function(response){
                $window.lert('Reply was sent Successfully!');
                    $state.go($state.current, {}, {reload: true});
            })
            .error(function(error){
                console.log(error);
            });
        }
    });