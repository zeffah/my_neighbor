
(function(){
  var 
   container = $('#report'),
   cache_width = container.width(),
   a4  =[ 595.28,  841.89];  // for a4 size paper width and height
   
  $('#create_pdf').on('click',function(){
   $('body').scrollTop(0);
   createPDF();
  });
  //create pdf
  function createPDF(){
   getCanvas().then(function(canvas){
    var 
    img = canvas.toDataURL("image/png"),
    doc = new jsPDF({
            unit:'px', 
            format:'a4'
          });     
          doc.addImage(img, 'JPEG', 20, 20);
          doc.save('reports.pdf');
          container.width(cache_width);
   });
  }
   
  // create canvas object
  function getCanvas(){
   container.width((a4[0]*1.33333) -80).css('max-width','none');
   return html2canvas(form,{
       imageTimeout:2000,
       removeContainer:true
      }); 
  }
   
}());


// $scope.printDocument = function(){
//   var pdf = new jsPDF('p','pt','a4');
//   pdf.addHTML(document.body,  function() {
//     pdf.save('web.pdf');
//   });
// }
