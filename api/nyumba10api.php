<?php
    header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    /**include the config file**/
	require 'config.php';

    /** Switch Case to Get Action from controller **/
	switch ($_GET['action']) {
		case 'send_message':
			sendMessage();
			break;
		case 'send_broadcast':
			sendBroadcast();
			break;
		case 'display_broadcast':
			displayBroadcast();
				break;
		case 'agent_feedback':
			agentFeedback();
				break;
		case 'update':
			update();
				break;
		case 'login':
			login();
				break;
        case 'admin_login':
			admin_login();
				break;	
        case 'item_delete':
			itemDelete();
				break;
        case 'agent_broadcast':
			agent_broadcast();
				break;
		default:
			'invalid action';
			break;
	}

    /**php functions for the above switch statement**/
	/**function to log in user if number exist**/
    
    function login(){
        $data = json_decode(file_get_contents("php://input"));
        $phone_number = $data->phone_number;
        $userData = array('id'=>'', 'fullname'=>'', 'house_id'=>'', 'phoneNo'=>'', 'nationalID'=>'');
        
        if(!empty($phone_number)){
            $phone_number = mysql_real_escape_string($phone_number);
            $query = 'SELECT id, house_id, fullname, phone, nat_id FROM tenants WHERE phone="'.$phone_number.'" LIMIT 1';
            
            $result = mysql_query($query);
            
            $match = mysql_num_rows($result);
            $res = mysql_fetch_assoc($result);
            
            if($match > 0){
                $userData['id'] = $res['id'];
                $userData['fullname'] = $res['fullname'];
                $userData['house_id'] = $res['house_id'];
                $userData['phoneNo'] = $res['phone'];
                $userData['nationalID'] = $res['nat_id'];
                
                print_r('{"userData":'.json_encode($userData).',"error":{"code":"1", "message": "user exist"}}');
            }
            else{
                //phone number doesnt exist
                print_r('{
                "userData":'.json_encode($userData).',
                "error":{"code":"0", "message": "Phone Number not exist<br/>Please contact your Agent for assistance. "}}');
            }
        }
        else{
            print_r('{"userData":'.json_encode($userData).', 
				"error": {"code":"005", "message": "Unknown error occurred!"}}');
        }
    }

function admin_login(){
    $data = json_decode(file_get_contents("php://input"));
    $username = mysql_real_escape_string($data->username);
    $password = mysql_real_escape_string($data->password);
    $role = mysql_real_escape_string($data->role);
    
    $userData = array('id'=>'', 'username'=>'', 'fullname'=>'', 'phone'=>'');
     $query = 'SELECT id, username, password, fullname, phone, role FROM admins WHERE username="'.$username.'" AND password="'.$password.'" AND role="'.$role.'" LIMIT 1';
    
    $result = mysql_query($query);

    $match = mysql_num_rows($result);
    $res = mysql_fetch_assoc($result);
    if($match > 0){
        $userData['id'] = $res['id'];
        $userData['username'] = $res['username'];
        $userData['fullname'] = $res['fullname'];
        $userData['phone'] = $res['phone'];
        $userData['role'] = $res['role'];

        print_r('{"userData":'.json_encode($userData).',"error":{"code":"1", "message": "Login successful"}}');
    }
    else{
        print_r('{
        "userData":'.json_encode($userData).',
        "error":{"code":"0", "message": "Login Error. Please try again"}}');
    }
}

function sendMessage(){
    $data = json_decode(file_get_contents("php://input"));
    $sender_id = $data->sender_id;
    $recipient_id = $data->recipient_id;
    $title = $data->title;
    $content = $data->msg_content;
    $ticket_no = $data->ticket_no;
    
    
    print_r($data);
    
     
        $query = 'INSERT INTO message (sender_id, recipient_id, msg_content, ticket_no, title) values ('.$sender_id.', '.$recipient_id.', "'.$content.'", "'.$ticket_no.'", "'.$title.'")';
        
        $result = mysql_query($query);
		if ($result) {
			$dataArray = array('msg'=>"message sent successfully");
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}else{
			$dataArray = array('error'=>"error in inserting record".mysql_error());
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}
}

function sendBroadcast(){
    $data = json_decode(file_get_contents("php://input"));
    $sender_id = $data->sender_id;
    $recipient = $data->recipient;
    $category = $data->category;
    $content = $data->msg_content;
    
    print_r($data);
    
     
        $query = 'INSERT INTO broadcast (sender_id, recipient, category, msg_content) values ('.$sender_id.', "'.$recipient.'", "'.$category.'", "'.$content.'")';
        
        $result = mysql_query($query);
		if ($result) {
			$dataArray = array('msg'=>"message broadcast successfully");
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}else{
			$dataArray = array('error'=>"error in inserting record".mysql_error());
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}
}

function displayBroadcast(){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->me;
    $house_id = $data->house_id;
    $broadcast = array();
    
    if(!empty($id)){
        $id = mysql_real_escape_string($id);
        
        $sql = "SELECT b.id as msgID, b.msg_content, b.createdAt, t.fullname, t.phone FROM broadcast b JOIN tenants t ON b.sender_id = t.id WHERE b.sender_id!='".$id."' AND b.recipient = 'neighbors' AND t.house_id = '".$house_id."' ORDER BY b.id DESC";

        $result = mysql_query($sql);
        
        while($rows = mysql_fetch_array($result)){
            $broadcast[] = array(
               'id'=>$rows['msgID'], 'sender'=>$rows['fullname'], 'brContent'=>$rows['msg_content'],  'phone'=>$rows['phone'], 'ntCreatedAt'=>$rows['createdAt']
            ); 
        }
          print_r (json_encode($broadcast));
    }
}

function agentFeedback(){
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->me;
    $feedback = array();
    
    if(!empty($id)){
        $id = mysql_real_escape_string($id);
        
        $sql = "SELECT id, msg_content, reply, createdAt FROM broadcast WHERE sender_id = '".$id."' AND reply !='NULL' AND seen = 0 ORDER BY id DESC ";

        $result = mysql_query($sql);
        
        while($rows = mysql_fetch_array($result)){
            $feedback[] = array(
                'id'=>$rows['id'], 'reply'=>$rows['reply'], 'agContent'=>$rows['msg_content'], 'agCreatedAt'=>$rows['createdAt']
            ); 
        }
          print_r (json_encode($feedback));
    }
}

function itemDelete(){
    $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->id);
    $delFrom = mysql_real_escape_string($data->group);
    
    switch($delFrom){
        case "My Messages":
            $sql = "UPDATE message SET seen = 1 WHERE id = '".$id."'";
            
            $result = mysql_query($sql);
            if($result){
                $dataArray = array('code'=>'1', 'response'=>'success');
                $jsonData = json_encode($dataArray);
                print_r($jsonData);  
            }else{
                $dataArray = array('error'=>"error in inserting record: ".mysql_error());
                $jsonData = json_encode($dataArray);
                print_r($jsonData);
            }
            break;
        case "Notifications":
        case "Agent Feeback":
            $sql = "UPDATE broadcast set seen = 1 WHERE id = '".$id."'";
            
            $result = mysql_query($sql);
            if($result){
                $dataArray = array('code'=>'1', 'response'=>'success');
                $jsonData = json_encode($dataArray);
                print_r($jsonData);  
            }else{
                $dataArray = array('error'=>"error in inserting record: ".mysql_error());
                $jsonData = json_encode($dataArray);
                print_r($jsonData);
            }
            break;
        default:
            echo 'Invalid request';
            break;
    }
}

function agent_broadcast(){
    $data = json_decode(file_get_contents("php://input"));
    $house_id = mysql_real_escape_string($data->house_id);
    
    $output = array();
    
    $sql = "SELECT at.agent_id AS agent, at.content, at.createdAt, ah.house_id, a.fullname
            FROM agent_to_tenants at
            JOIN admins a ON at.agent_id = a.id
            JOIN agent_house ah ON ah.agent_id = a.id
            WHERE ah.house_id = '".$house_id."'";
    
    $result = mysql_query($sql);
    
    while($row = mysql_fetch_array($result)){
        $output[] = array(
            'broadContent'=>$row['content'], 'sender_id'=>$row['agent'], 'sender_name'=>$row['fullname'], 'brCreatedAt'=>$rows['createdAt']
        );
    }
    print_r(json_encode($output));
}

?>
