<?php
     header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    /**include the config file**/
	require 'config.php';

    /** Switch Case to Get Action from controller **/
	switch ($_GET['action']) {
		case 'register_tenant':
			register_tenant();
			break;
		case 'list_agents':
			list_agents();
			break;
		case 'assign_house':
			assign_house();
				break;
        case 'register_house':
            register_house();
                break;
        case 'update_house':
            update_house();
                break;
		case 'list_houses':
			list_houses();
				break;
        case 'list_units':
			list_units();
				break;
        case 'my_houses':
			myHouses();
				break;
		case 'register_units':
			register_units();
				break;
        case 'update_unit':
			update_unit();
				break;
		case 'login':
			login();
				break;
        case 'admin_login':
			admin_login();
				break;	
        case 'agent_tenants':
            agent_tenants();
            break;
        case 'broadcast':
            broadcast();
            break;
        case 'del_tenant':
            del_tenant();
            break;
        case 'reassignHouse':
            reassignHouse();
                break;
        case 'myAgents':
            myAgents();
                break;
        case 'reply':
            reply();
            break;
        case 'updates':
            updates();
            break;
        case 'complains':
            complains();
            break;
        case 'recommendations':
            recommendations();
            break;
        case 'admin_message':
        admin_message();
        break;
            
        case 'admin_comm':
        admin_comm();
        break;
        case 'broadcast_to_agents':
         broadcast_to_agents();
        break;
        case 'editTenant':
         editTenant();
        break; 
        case 'selectTenant':
         selectTenant();
        break;
        case 'landlordHouses':
            landlordHouses();
            break;
		default:
			'invalid action';
			break;
	}

function register_tenant(){
    $data = json_decode(file_get_contents("php://input"));
    $firstname = mysql_real_escape_string($data->firstname);
    $lastname = mysql_real_escape_string($data->lastname);
    $phone = mysql_real_escape_string($data->phone);
    $natID = mysql_real_escape_string($data->nationalID);
    $house_id = mysql_real_escape_string($data->house_id);
    $unit_id = mysql_real_escape_string($data->unit_id);
    
    $query = 'INSERT INTO tenants (unit_id, house_id, fullname, nat_id, phone) values ('.$unit_id.','.$house_id.', "'.$firstname.' '.$lastname.'", "'.$natID.'", "'.$phone.'")';
        
        $result = mysql_query($query);
		if ($result) {
			$dataArray = array('msg'=>"message sent successfully", 'unitID'=>$unit_id);
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}else{
			$dataArray = array('error'=>"error in inserting record".mysql_error());
			$jsonData = json_encode($dataArray);
			print_r($jsonData);
		}
}
/*
** A landlord registers his houses
*/
function register_house(){
    $data = json_decode(file_get_contents("php://input"));
    $capacity = mysql_real_escape_string($data->capacity);
    $landlord_id = mysql_real_escape_string($data->landlord);
    $houseName = mysql_real_escape_string($data->houseName);
    
    $sql = "INSERT INTO houses (house_name, capacity, landlord_id) values('".$houseName."', '.$capacity.', '.$landlord_id.')";
    $result = mysql_query($sql);
    
    if ($result) {
        $house_id = mysql_insert_id();
        $dataArray = array('msg'=>"successfully", 'houseID'=>$house_id);
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }else{
        $dataArray = array('error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }
    
}

/*
**A landlord assigns his house to a particular agent
*/
function assign_house(){
    $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $landlord_id = mysql_real_escape_string($data->landlord_id);
    $house_id = mysql_real_escape_string($data->house_id);
    
    $sql = "INSERT INTO agent_house (agent_id, house_id) values('.$agent_id.', '.$house_id.')";
    $result = mysql_query($sql);
    
    if ($result) {
        $house_id = mysql_insert_id();
        $dataArray = array('msg'=>"successfully", 'houseID'=>$house_id);
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }else{
        $dataArray = array('error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }
    
}

/*
** Update house if assigned to agent
*/
function update_house(){
    $data = json_decode(file_get_contents("php://input"));
    $house_id = mysql_real_escape_string($data->house_id);
    $status = mysql_real_escape_string($data->status);
    
    $sql = "UPDATE houses SET assigned = '.$status.' WHERE id='".$house_id."'";
    $result = mysql_query($sql);
    
    if($result){
        $dataArray = array('code'=>'1', 'response'=>'success');
        $jsonData = json_encode($dataArray);
        print_r($jsonData);  
    }else{
        $dataArray = array('error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }
}

/*
** Re-assign a house
*/
function reassignHouse(){
    $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->id);
    $sql = "DELETE FROM agent_house where id='".$id."'";
//    $jsonRes = array();
    $result = mysql_query($sql);
    if($result){
        $jsonRes = array('success'=> '1', 'msg'=> 'Delete successful');
        
        print_r(json_encode($jsonRes));
    }else{
        $jsonRes = array('success'=> '0', 'msg'=> 'Delete successful: '.mysql_error());
        print_r(json_encode($jsonRes));
    }
}
/*
** A function to register a house unit into the system
*/
function register_units(){
    $data = json_decode(file_get_contents("php://input"));
    $unit_no = mysql_real_escape_string($data->unit);
    $house_id = mysql_real_escape_string($data->house_id);
    $status = mysql_real_escape_string($data->status);
    
    $sql = "INSERT INTO unit (house_no, housename_id, status) values('".$unit_no."', '".$house_id."', '.$status.')";
    
    $result = mysql_query($sql);
    if($result){
        $dataArray = array('response'=>'success');
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }else{
        $dataArray = array('house_no'=>$unit_no, 'error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }
}
/*
**List of all agents in the system
*/
function list_agents(){
    $agents = array();
    $sql = 'SELECT id, fullname, company FROM admins WHERE role = "agent" ';
    
    $result = mysql_query($sql);
        
        while($rows = mysql_fetch_array($result)){
            $agents[] = array(
                'id'=>$rows['id'], 'fullname'=>$rows['fullname'],  'phone'=>$rows['phone'], 'company'=>$rows['company']
            ); 
        }
        print_r(json_encode($agents));
}

/*
**List agent asigned to which house for which landlord
*/
function myAgents(){
     $data = json_decode(file_get_contents("php://input"));
    $landlord_id = mysql_real_escape_string($data->landlord_id);
    $myHouseToAgents = array();
    
    $sql = "SELECT ah.id, ah.house_id, h.house_name, a.id as agentId, a.company, a.fullname FROM agent_house ah JOIN houses h ON ah.house_id = h.id JOIN admins a ON ah.agent_id = a.id WHERE h.landlord_id = '".$landlord_id."'";
    $result = mysql_query($sql);
    while($rows = mysql_fetch_assoc($result)){
        $myHouseToAgents[] = array(
        'id'=>$rows['id'], 'agent_id'=>$rows['agentId'], 'house_id'=>$rows['house_id'], 'houseName'=>$rows['house_name'], 'agentCo'=>$rows['company'], 'repName'=>$rows['fullname']
        );
    }
    print_r(json_encode($myHouseToAgents));
}
/*
** This lists houses for tenants
*/
function list_houses(){ //this is for agents (houses assigned to an agent)
    $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $houses = array();
    $sql = "SELECT h.id, h.house_name, h.landlord_id, l.id AS landlord_id, l.fullname, l.phone FROM houses h JOIN admins l ON h.landlord_id = l.id JOIN agent_house ah ON ah.house_id = h.id  WHERE ah.agent_id='".$agent_id."'";
    $result = mysql_query($sql);
    
    while($rows = mysql_fetch_assoc($result)){
        $houses[] = array(
        'id'=>$rows['id'], 'landlord_id'=>$rows['landlord_id'], 'houseName'=>$rows['house_name'], 'landlord'=>$rows['landlord_id'], 'landlord_name'=>$rows['fullname'], 'landlord_phone'=>$rows['phone']
        );
    }
    print_r(json_encode($houses));
}
/*
**Houses belonging to logged in landlord
*/
function myHouses(){//This is for landlords only (unassigned houses only)
     $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->landlord_id);
    
    $lHouses = array();
    
    $sql = "SELECT id, house_name, landlord_id FROM houses WHERE assigned = '0' AND landlord_id='".$id."'";
    $result = mysql_query($sql);
    
    while($rows = mysql_fetch_assoc($result)){
        $lHouses[] = array(
        'id'=>$rows['id'], 'houseName'=>$rows['house_name'], 'landlord'=>$rows['landlord_id']
        );
    }
    print_r(json_encode($lHouses));
}

/*This list all houses belonging to an agent(both assigned and unassigned) */
    function landlordHouses(){//This is for landlords only (unassigned houses only)
         $data = json_decode(file_get_contents("php://input"));
        $id = mysql_real_escape_string($data->landlord_id);

        $lHouses = array();

        $sql = "SELECT id, house_name, landlord_id FROM houses WHERE landlord_id='".$id."'";
        $result = mysql_query($sql);

        while($rows = mysql_fetch_assoc($result)){
            $lHouses[] = array(
            'id'=>$rows['id'], 'houseName'=>$rows['house_name'], 'landlord'=>$rows['landlord_id']
            );
        }
        print_r(json_encode($lHouses));
    }

/*
** List of all units under certain(selected) house
*/
function list_units(){
    $data = json_decode(file_get_contents("php://input"));
    $house_id = mysql_real_escape_string($data->house_id);
    $units = array();
    $sql = "SELECT id, house_no, status FROM unit WHERE housename_id='".$house_id."' AND status= '0' ";
    $result = mysql_query($sql);
    
    while($rows = mysql_fetch_assoc($result)){
        $units[] = array(
        'id'=>$rows['id'], 'houseNo'=>$rows['house_no'], 'status'=>$rows['status']
        );
    }
    print_r(json_encode($units));
}
/*
**function to update the status of a unit if already assigned to tenant
*/
function update_unit(){
    $data = json_decode(file_get_contents("php://input"));
    $unit_id = mysql_real_escape_string($data->unit_id);
    $status = mysql_real_escape_string($data->status);
    
    $sql = "UPDATE unit SET status = '.$status.' WHERE id='".$unit_id."'";
    $result = mysql_query($sql);
    
    if($result){
        $dataArray = array('code'=>'1', 'response'=>'success');
        $jsonData = json_encode($dataArray);
        print_r($jsonData);  
    }else{
        $dataArray = array('error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($dataArray);
        print_r($jsonData);
    }
}

function agent_tenants(){
    $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    
    $tenants = array();
    
    $sql = "SELECT t.id, t.fullname, t.nat_id, t.phone, h.house_name, t.unit_id, u.house_no, u.status FROM tenants t JOIN houses h ON t.house_id = h.id
    JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE ah.agent_id = '".$agent_id."'";
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $tenants[] = array(
        'id'=>$rows['id'], 'fullname'=>$rows['fullname'], 'national_id'=> $rows['nat_id'], 'phone'=>$rows['phone'], 'houseName'=>$rows['house_name'],  'unit_id'=>$rows['unit_id'], 'houseNo'=>$rows['house_no']);
    }
    print_r(json_encode($tenants));
}
function del_tenant(){
    $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->id);
    $sql = "DELETE FROM tenants where id='".$id."'";
//    $jsonRes = array();
    $result = mysql_query($sql);
    if($result){
        $jsonRes = array('success'=> '1', 'msg'=> 'Delete successful');
        
        print_r(json_encode($jsonRes));
    }else{
        $jsonRes = array('success'=> '0', 'msg'=> 'Delete successful: '.mysql_error());
        print_r(json_encode($jsonRes));
    }
}

function broadcast(){
    $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    
    $bCast = array();
    $sql = "SELECT b.id, b.sender_id, b.msg_content, t.house_id, t.fullname, t.nat_id, t.phone, ah.agent_id, h.house_name, u.house_no FROM broadcast b JOIN tenants t ON b.sender_id = t.id JOIN houses h ON t.house_id = h.id JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE reply IS NULL AND recipient = 'agent' AND ah.agent_id = '".$agent_id."'";
    
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $bCast[] = array(
        'msg_id'=>$rows['id'], 'sender_id'=>$rows['sender_id'], 'msg_content'=>$rows['msg_content'], 'hseID'=>$rows['house_id'], 'agent_id'=>$rows['agent_id'],
        'sender_name'=>$rows['fullname'], 'sender_national_id'=>$rows['nat_id'], 'sender_phone'=>$rows['phone'], 'houseName'=>$rows['house_name'], 'house_no'=>$rows['house_no']);
    }
    print_r(json_encode($bCast));
}
function reply(){
    $data = json_decode(file_get_contents("php://input"));
    $msg =  mysql_real_escape_string($data->reply);
    $msg_id = mysql_real_escape_string($data->msg_id);
    
    $sql = "UPDATE broadcast SET reply = '".$msg."' WHERE id = '".$msg_id."'";
    $result = mysql_query($sql);
    
    if($result){
        $jsonReply = array('code'=>'1', 'msg'=>'reply successful');
        print_r($jsonReply);
    }else{
        $jsonReply = array('code'=>'0', 'msg'=>'reply failed:  '.mysql_error());
        print_r($jsonReply);
    }
}

function complains(){
     $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $complain = array();
    $sql = "SELECT b.id, b.sender_id, b.msg_content, b.reply, t.house_id, t.fullname, t.nat_id, t.phone, ah.agent_id, h.house_name, u.house_no FROM broadcast b JOIN tenants t ON b.sender_id = t.id JOIN houses h ON t.house_id = h.id JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE category = 'complain' AND recipient = 'agent' AND ah.agent_id = '".$agent_id."'";
    
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $complain[] = array(
        'msg_id'=>$rows['id'], 'sender_id'=>$rows['sender_id'], 'msg_content'=>$rows['msg_content'], 'reply'=>$rows['reply'], 'hseID'=>$rows['house_id'], 'agent_id'=>$rows['agent_id'],
        'sender_name'=>$rows['fullname'], 'sender_national_id'=>$rows['nat_id'], 'sender_phone'=>$rows['phone'], 'houseName'=>$rows['house_name'], 'house_no'=>$rows['house_no']);
    }
    print_r(json_encode($complain));
}
function updates(){
     $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $update = array();
    $sql = "SELECT b.id, b.sender_id, b.msg_content, b.reply, t.house_id, t.fullname, t.nat_id, t.phone, ah.agent_id, h.house_name, u.house_no FROM broadcast b JOIN tenants t ON b.sender_id = t.id JOIN houses h ON t.house_id = h.id JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE category = 'update' AND recipient = 'agent' AND ah.agent_id = '".$agent_id."'";
    
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $update[] = array(
        'msg_id'=>$rows['id'], 'sender_id'=>$rows['sender_id'], 'msg_content'=>$rows['msg_content'], 'reply'=>$rows['reply'], 'hseID'=>$rows['house_id'], 'agent_id'=>$rows['agent_id'],
        'sender_name'=>$rows['fullname'], 'sender_national_id'=>$rows['nat_id'], 'sender_phone'=>$rows['phone'], 'houseName'=>$rows['house_name'], 'house_no'=>$rows['house_no']);
    }
    print_r(json_encode($update));
}

function recommendations(){
     $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $recommendation = array();
    $sql = "SELECT b.id, b.sender_id, b.msg_content, b.reply, t.house_id, t.fullname, t.nat_id, t.phone, ah.agent_id, h.house_name, u.house_no FROM broadcast b JOIN tenants t ON b.sender_id = t.id JOIN houses h ON t.house_id = h.id JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE category = 'recommendation' AND recipient = 'agent' AND ah.agent_id = '".$agent_id."'";
    
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $recommendation[] = array(
        'msg_id'=>$rows['id'], 'sender_id'=>$rows['sender_id'], 'msg_content'=>$rows['msg_content'], 'reply'=>$rows['reply'], 'hseID'=>$rows['house_id'], 'agent_id'=>$rows['agent_id'],
        'sender_name'=>$rows['fullname'], 'sender_national_id'=>$rows['nat_id'], 'sender_phone'=>$rows['phone'], 'houseName'=>$rows['house_name'], 'house_no'=>$rows['house_no']);
    }
    print_r(json_encode($recommendation));
}

function admin_message(){
    $data = json_decode(file_get_contents("php://input"));
    $title = mysql_real_escape_string($data->title);
    $message = mysql_real_escape_string($data->message);
    $sender_id = mysql_real_escape_string($data->sender);
    $recipient_id = mysql_real_escape_string($data->recipient);
    
    $sql = "INSERT INTO admin_communication (sender_id, recipient_id, title, content) VALUES('.$sender_id.', '.$recipient_id.', '".$title."', '".$message."')";
    
    $adminResponse = array();
    
    $result = mysql_query($sql);
    if($result){
        $adminResponse = array('response'=>'success');
        $jsonData = json_encode($adminResponse);
        print_r($jsonData);
    }else{
        $adminResponse = array('house_no'=>$unit_no, 'error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($adminResponse);
        print_r($jsonData);
    }
}

function admin_comm(){
    $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->user_id);
    $resp = array();
    $sql = "SELECT ac.id, ac.title, ac.content, a.fullname, a.company, a.phone, a.role FROM admin_communication ac JOIN admins a ON ac.sender_id = a.id WHERE ac.recipient_id = '".$id."'";
    $result = mysql_query($sql);
    
    while($rows = mysql_fetch_array($result)){
        $resp[] = array(
            'mgsID'=>$rows['id'], 'title'=>$rows['title'], 'content'=>$rows['content'], 'fullname'=>$rows['fullname'], 'phone'=>$rows['phone'],
            'role'=>$rows['role']);
    }
    print_r(json_encode($resp));
}

function broadcast_to_agents(){
   $data = json_decode(file_get_contents("php://input"));
    $content = mysql_real_escape_string($data->content);
    $sender_id = mysql_real_escape_string($data->sender);
    $responseData = array();
    $query = "INSERT INTO agent_to_tenants(agent_id, content) values('.$sender_id.', '".$content."')";
    $rs = mysql_query($query);
    if($rs){
       $responseData = array('response'=>'success sent broadcast');
        $jsonData = json_encode($responseData);
        print_r($jsonData); 
    }else{
        $responseData = array('response'=>'Error broadcasting. Try again!!');
        $jsonData = json_encode($responseData);
        print_r($jsonData);
    }  
}

function selectTenant(){
    $data = json_decode(file_get_contents("php://input"));
    $agent_id = mysql_real_escape_string($data->agent_id);
    $tenant_id = mysql_real_escape_string($data->tenant_id);
    
    $tenant = array();
    
    $sql = "SELECT t.id, t.fullname, t.nat_id, t.phone, h.house_name, t.unit_id, u.house_no, u.status FROM tenants t JOIN houses h ON t.house_id = h.id
    JOIN unit u ON t.unit_id = u.id JOIN agent_house ah ON ah.house_id = h.id WHERE ah.agent_id = '".$agent_id."' AND t.id = '".$tenant_id."'";
    $result = mysql_query($sql);
    while($rows = mysql_fetch_array($result)){
        $tenant = array(
        'id'=>$rows['id'], 'fullname'=>$rows['fullname'], 'national_id'=> $rows['nat_id'], 'phone'=>$rows['phone'], 'houseName'=>$rows['house_name'],  'unit_id'=>$rows['unit_id'], 'houseNo'=>$rows['house_no']);
    }
    print_r(json_encode($tenant));
}

function editTenant(){
    $data = json_decode(file_get_contents("php://input"));
    $id = mysql_real_escape_string($data->id);
    $phone = mysql_real_escape_string($data->phone);
    $house = mysql_real_escape_string($data->house);
    $unit = mysql_real_escape_string($data->unit);
    
    $sql = "UPDATE tenants SET phone = '".$phone."', unit_id = '".$unit."', house_id = '".$house."' WHERE id = '".$id."'";
    
    $result = mysql_query($sql);
    if($result){
        $res = array('code'=>'1', 'response'=>'Tenant Updated successfully');
        $jsonData = json_encode($res);
        print_r($jsonData);
    }else{
        $res = array('house_no'=>$unit_no, 'error'=>"error in inserting record: ".mysql_error());
        $jsonData = json_encode($res);
        print_r($jsonData);
    }
}

?>